﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sights.Models
{
    public class ShowingDto
    {
        public int Id { get; set; }

        public string Author { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }
        public string Genre { get; set; }

        public string Duration { get; set; }

        public string PosterUrl { get; set; }

        public DateTime Date { get; set; }

        public string Type { get; set; }

        public Seance[] Seances { get; set; }

        public Location Location { get; set; }

        public static ShowingDto CreateFrom(Showing showing)
        {
            
            var result = new ShowingDto()
            {
                Id = showing.Id,
                Author = showing.Author,
                Name = showing.Name,
                Description = showing.Description,
                Genre = showing.Genre,
                Duration = showing.Duration,
                PosterUrl = showing.PosterUrl,
                Date = showing.Date,
                Type = showing.Type
            };

            result.Seances = showing.Seances.ToArray();
            result.Location = showing.Seances.First().Location;
            return result;
        }
    }
}