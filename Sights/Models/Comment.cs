﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sights.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public int Cinema { get; set; }

        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}