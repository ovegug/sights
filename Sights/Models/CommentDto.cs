﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sights.Models
{
    public class CommentDto
    {
        public string Text { get; set; }

        public string Username { get; set; }
    }
}