﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Sights.Models
{
    public class Seance
    {
        [Key]
        public int Id { get; set; }

        public int ShowingId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public Showing Showing { get; set; }

        public virtual int LocationId { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual Location Location { get; set; }

        [Display(Name="Время сеанса")]
        public DateTime Time { get; set; }
        [Display(Name="Категория")]
        public string Type { get; set; }
    }
}