﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Sights.Models
{
    public class Location
    {
        public Location()
        {
            Seances = new HashSet<Seance>();
        }

        public int Id { get; set; }
        [Display(Name="Наименование")]
        public string Name { get; set; }

        [Display(Name="Долгота")]
        public double? Longitude { get; set; }
        [Display(Name="Широта")]
        public double? Latitude { get; set; }

        [Display(Name="Адрес")]
        public string Address { get; set; }
        [Display(Name="Описание")]
        public string Description { get; set; }

        [Display(Name="Категория")]
        public string Type { get; set; }


        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Seance> Seances { get; set; }
    }
}