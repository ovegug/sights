﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Sights.Models
{
    public class Showing
    {
        public Showing()
        {
            Seances = new HashSet<Seance>();
        }

        public int Id { get; set; }

        [Display(Name = "Автор")]
        public string Author { get; set; }
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Жанр")]
        public string Genre { get; set; }

        [Display(Name = "Продолжительность")]
        public string Duration { get; set; }

        [Display(Name = "Ссылка на постер")]
        public string PosterUrl { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime Date { get; set; }

        [Display(Name = "Категория")]
        public string Type { get; set; }


        [JsonIgnore]
        [IgnoreDataMember]
        public virtual ICollection<Seance> Seances { get; set; }
    }
}