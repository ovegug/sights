﻿var app = angular.module('app', ['ngRoute','ngCookies', 'todayFilms', 'seance', 'map', 'comments', 'theater']);


app.config(['$provide', '$routeProvider', '$httpProvider', function ($provide, $routeProvider, $httpProvider) {

    //================================================
    // Ignore Template Request errors if a page that was requested was not found or unauthorized.  The GET operation could still show up in the browser debugger, but it shouldn't show a $compile:tpload error.
    //================================================
    //$provide.decorator('$templateRequest', ['$delegate', function ($delegate) {
    //    var mySilentProvider = function (tpl, ignoreRequestError) {
    //        return $delegate(tpl, true);
    //    }
    //    return mySilentProvider;
    //}]);

    ////================================================
    //// Add an interceptor for AJAX errors
    ////================================================
    //$httpProvider.interceptors.push(['$q', '$location', function ($q, $location) {
    //    return {
    //        'responseError': function (response) {
    //            if (response.status === 401)
    //                $location.url('/signin');
    //            return $q.reject(response);
    //        }
    //    };
    //}]);


    //================================================
    // Routes
    //================================================
    $routeProvider.when('/seance/:id', {
        templateUrl: 'Home/Seance',
        controller: 'seanceCtrl'
    });

    $routeProvider.when('/cinema', {
        templateUrl: 'Home/Cinema',
        controller: 'todayFilmsCtrl'
    });

    $routeProvider.when('/theater', {
        templateUrl: 'Home/Theater',
        controller: 'theaterCtrl'
    });

    $routeProvider.when('/concert', {
        templateUrl: 'Home/Concert',
        controller: 'concertCtrl'
    });

    $routeProvider.when('/museum', {
        templateUrl: 'Home/Museum',
        controller: 'museumCtrl'
    });

    $routeProvider.when('/map', {
        templateUrl: 'Home/Map',
        controller: 'mapCtrl'
    });

    $routeProvider.when('/comments/:id', {
        templateUrl: 'Home/Comments',
        controller: 'commentsCtrl'
    });

    $routeProvider.otherwise({ redirectTo: '/map' });
}]);

//app.run(['$http', '$cookies', '$cookieStore', function ($http, $cookies, $cookieStore) {
//    $http.defaults.headers.common.Authorization = 'Bearer ' + $cookieStore.get('_Token');
//    $http.defaults.headers.common.RefreshToken = $cookieStore.get('_RefreshToken');
//}]);


app.run(['$rootScope', '$http', '$cookies', '$cookieStore', function ($rootScope, $http, $cookies, $cookieStore) {
    $rootScope.showSign = function () {
        $('#signin').modal();
    };

    $rootScope.showSignup = function () {
        $('#signup').modal();
    };

    $rootScope.logout = function () {

        $http.post('/api/Account/Logout')
            .success(function (data, status, headers, config) {
                $http.defaults.headers.common.Authorization = null;
                $http.defaults.headers.common.RefreshToken = null;
                $cookieStore.remove('_Token');
                $cookieStore.remove('_RefreshToken');
                $rootScope.username = '';
                $rootScope.loggedIn = false;
                window.location = '#/signin';
            });

    }
    $rootScope.$on('$locationChangeSuccess', function (event) {
        if ($http.defaults.headers.common.RefreshToken != null) {
            var params = "grant_type=refresh_token&refresh_token=" + $http.defaults.headers.common.RefreshToken;
            $http({
                url: '/Token',
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: params
            })
            .success(function (data, status, headers, config) {
                $http.defaults.headers.common.Authorization = "Bearer " + data.access_token;
                $http.defaults.headers.common.RefreshToken = data.refresh_token;

                $cookieStore.put('_Token', data.access_token);
                $cookieStore.put('_RefreshToken', data.refresh_token);

                $http.get('GetCurrentUserName')
                    .success(function (data, status, headers, config) {
                        if (data != "null") {
                            $rootScope.username = data.replace(/["']{1}/gi, "");//Remove any quotes from the username before pushing it out.
                            $rootScope.loggedIn = true;
                        }
                        else
                            $rootScope.loggedIn = false;
                    });


            })
            .error(function (data, status, headers, config) {
                $rootScope.loggedIn = false;
            });
        }
    });
}]);