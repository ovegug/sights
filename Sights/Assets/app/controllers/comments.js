﻿angular.module('comments', ['ngRoute', 'ngCookies'])
.controller('commentsCtrl', ['$rootScope', '$scope', '$http', '$routeParams', '$cookieStore', function ($rootScope, $scope, $http, $routeParams, $cookieStore) {
    $scope.comments = [];

    $scope.loadComments = function () {
        $scope.cinema = $routeParams.id;

        $http.get('api/CinemaComment/' + $routeParams.id).success(function (data) {
            $scope.comments = data;
        });
    };

    $scope.loadComments();


    $scope.addComment = function () {
        $http.post('api/Comments/', { Text: $scope.comment, Cinema: $scope.cinema }, {
            headers: {
                'Authorization': 'Bearer ' + $cookieStore.get('_Token'),
                'RefreshToken': $cookieStore.get('_RefreshToken')
            }
        }).success(function (data) {
            $scope.comments.push(data);
            $scope.comment = null;
        });
    };
}])



    .controller('signInCtrl', ['$scope', '$rootScope', '$http', '$cookies', '$cookieStore', '$location', '$routeParams', function ($scope, $rootScope, $http, $cookies, $cookieStore, $location, $routeParams) {
        $scope.close = function () {
            $('#signin').modal('hide');
            $scope.errorMessage = null;
        };

        $scope.signIn = function () {
            $scope.showMessage = false;
            var params = "grant_type=password&username=" + $scope.username + "&password=" + $scope.password;
            $http({
                url: '/Token',
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: params
            })
            .success(function (data, status, headers, config) {
                $rootScope.loggedIn = true;

                $scope.message = $routeParams.message;

                $http.defaults.headers.common.Authorization = "Bearer " + data.access_token;
                $http.defaults.headers.common.RefreshToken = data.refresh_token;

                $cookieStore.put('_Token', data.access_token);
                //$http.get("api/Account/UserRoles")
                //.success(function (response) {
                //    //var admin = response.indexOf("Администратор") > -1;
                //    console.log(response);
                //    //console.log(admin);
                //    //if (admin) {
                //    //    $rootScope.admin = true;
                //    //    //$location.path('/admin/users');
                //    //} else {
                //    //    $rootScope.admin = false;
                //    //    //$location.path('/wp/all');
                //    //}
                //    $scope.close();
                //});

                $http.get("api/Account/GetCurrentUserName")
                .success(function (response) {
                    console.log(response);
                    $rootScope.username = response;
                });
                $scope.close();
            })
            .error(function (data, status, headers, config) {
                $scope.message = $routeParams.message;

                $scope.errorMessage = data.error_description.replace(/["']{1}/gi, "");
            });
        }
    }])


    .controller('registerCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.close = function () {
            $('#signup').modal('hide');
            $scope.successMessage = null;
        };
        $scope.register = function () {
            var params = {
                Email: $scope.username,
                Password: $scope.password,
                ConfirmPassword: $scope.password
            };
            $http.post('/api/Account/Register', params)
            .success(function (data, status, headers, config) {
                $scope.successMessage = "Регистрация успешно пройдёна. Авторизуйтесь.";
            })
            .error(function (data, status, headers, config) {
                if (angular.isArray(data))
                    $scope.errorMessages = data;
                else
                    $scope.errorMessages = new Array(JSON.stringify(data).replace(/["']{1}/gi, ""));

            });
        }
    }]);