﻿angular.module('seance', ['ngMap', 'ngRoute'])
.controller('seanceCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    $scope.map = {
        center: [52.61022, 39.594719],
        zoom: 12
    };

    $scope.cinemas = [];

    $scope.loadFilmInfo = function () {
        $http.get('http://api.kinopoisk.cf/getSeance?cityID=486&filmID=' + $routeParams.id).success(function (data) {
            $scope.film = data;
            $scope.cinemas = data.items;
        });
    };

    $scope.loadFilmInfo();


    $scope.seanceInfo = function (evt, cinema) {
        $scope.cinemaInfo = cinema;
    };
}]);