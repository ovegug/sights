﻿angular.module('theater', [])
.controller('concertCtrl', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {
    $scope.items = [];

    $http.get('/api/Concerts').success(function (data) {
        $scope.items = data;
    });

    $scope.posterUrl = function (item) {
        return item.PosterUrl != null ? item.PosterUrl : 'http://www.6461530.ru/img/404.jpg';
    };

    $scope.showDetail = function (data) {
        $scope.detail = data;
        $('#detail').modal();
    };
}]);