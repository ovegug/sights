﻿angular.module('map', ['ngMap'])
.controller('mapCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.map = {
        center: [52.61022, 39.594719],
        zoom: 12
    };

    $scope.cinemas = [];

    $scope.others = [];

    $scope.load = function () {
        $http.get('/api/ApiLocations').success(function (data) {
            $scope.others = data;
        });

        $http.get('http://api.kinopoisk.cf/getCinemas?cityID=486').success(function (data) {
            $scope.cinemas = data.items;
        });
    };

    $scope.getIcon = function (type) {
        if(type == 'Театр')
            return 'Content/theater_icon.png';
        else if (type == 'Музей')
            return 'Content/museum_icon.png';
        else if (type == 'Концерт')
            return 'Content/concert_icon.png';
        return 'Content/film-roll.png';
    }

    $scope.showShowing = function (evt, showing) {
        var api = '/api/Locations/';
        var type = '';
        if (showing.Type == 'Театр')
            type = 'Theaters';
        else if (showing.Type == 'Музей')
            type = 'Museums';
        else if (showing.Type == 'Концерт')
            type = 'Concerts';

        var url = api + type + '/' + showing.Id;

        $http.get(url).success(function (data) {
            $scope.showing = showing;
            $scope.showing.items = data;

            $('#otherInfoShow').modal('show');
        });
    };

    $scope.showInfo = function (evt, cinemaID) {
        $http.get('http://api.kinopoisk.cf/getCinemaDetail?cinemaID=' + cinemaID).success(function (data) {
            $scope.cinemaInfo = data.cinemaDetail;
            $('#cinemaInfoShow').modal('show');
        });
    };

    $scope.load();

}]);