﻿angular.module('todayFilms', [])
.controller('todayFilmsCtrl', ['$scope', '$http', '$sce', function ($scope, $http, $sce) {
    $scope.films = [];

    var date = new Date().toLocaleDateString().replace('/', '.');

    $http.get('http://api.kinopoisk.cf/getTodayFilms?cityID=486&date=' + date).success(function (data) {
        $scope.films = data.filmsData;
    });

    $scope.showDetail = function (film) {
        $http.get('http://api.kinopoisk.cf/getFilm?filmID=' + film.id).success(function (data) {
            $scope.filmDetail = data;
            $('#filmDetail').modal();
        });
    };

    $scope.showTrailer = function (film) {
        $scope.filmDetail = film;
        $scope.filmDetail.videoURL = $sce.trustAsResourceUrl(film.videoURL);
        $('#filmTrailer').modal();
    };
}]);