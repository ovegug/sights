﻿using System.Web;
using System.Web.Optimization;

namespace Sights
{
    public class BundleConfig
    {
        // Дополнительные сведения об объединении см. по адресу: http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Используйте версию Modernizr для разработчиков, чтобы учиться работать. Когда вы будете готовы перейти к работе,
            // используйте средство построения на сайте http://modernizr.com, чтобы выбрать только нужные тесты.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                  "~/Scripts/moment.js",
                  "~/Scripts/ru.js",
                  "~/Scripts/bootstrap-datetimepicker.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/bs").Include(
                      "~/Content/bootstrap.css",
                  "~/Content/bootstrap-datetimepicker.css"));


            bundles.Add(new ScriptBundle("~/ng").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-cookies.min.js",
                "~/Scripts/ng-map.js",
                "~/Scripts/i18n/angular-locale_ru-ru.js",
                "~/Scripts/angular-route.js"));

            bundles.Add(new ScriptBundle("~/app").Include(
                "~/Assets/app/controllers/todayFilms.js",
                "~/Assets/app/controllers/comments.js",
                "~/Assets/app/controllers/concert.js",
                "~/Assets/app/controllers/theater.js",
                "~/Assets/app/controllers/museum.js",
                "~/Assets/app/controllers/seance.js",
                "~/Assets/app/controllers/map.js",
                "~/Assets/app/app.js"));
        }
    }
}
