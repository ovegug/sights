namespace Sights.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seance : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Seances");
            AddColumn("dbo.Seances", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Seances", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Seances");
            DropColumn("dbo.Seances", "Id");
            AddPrimaryKey("dbo.Seances", new[] { "ShowingId", "LocationId" });
        }
    }
}
