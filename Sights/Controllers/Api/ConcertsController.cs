﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Sights.Models;

namespace Sights.Controllers
{
    //[RoutePrefix("api/Concerts")]
    public class ConcertsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("api/Seances/Concerts")]
        public IQueryable<Seance> GetSeance(int id)
        {
            return db.Seances.Where(s => s.ShowingId == id);
        }


        [Route("api/Locations/Concerts/{id}")]
        public IQueryable<ShowingDto> GetLocationShowings(int id)
        {
            var showings = db.Locations.Where(l => l.Type == "Концерт" && l.Id == id).
                SelectMany(l => l.Seances.Select(s => s.Showing)).ToList();

            return showings.Select(s => ShowingDto.CreateFrom(s)).AsQueryable();
        }


        [Route("api/Locations/Concerts")]
        public IQueryable<Location> GetLocations()
        {
            return db.Locations.Where(l => l.Type == "Концерт");
        }
        // GET: api/Concerts
        public IQueryable<ShowingDto> GetShowings()
        {
            DateTime today = DateTime.Today;                    // earliest time today 
            DateTime tomorrow = DateTime.Today.AddDays(1);

            return db.Showings
                .Where(s => s.Type == "Концерт" && s.Seances.Any(seance => seance.Time >= today && seance.Time < tomorrow))
                .ToArray()
                .Select(s => ShowingDto.CreateFrom(s))
                .AsQueryable();
        }

        // GET: api/Concerts/5
        [ResponseType(typeof(Showing))]
        public IHttpActionResult GetConcerts(int id)
        {
            Showing showing = db.Showings.Find(id);
            if (showing == null)
            {
                return NotFound();
            }

            return Ok(showing);
        }

        // PUT: api/Concerts/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShowing(int id, Showing showing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != showing.Id)
            {
                return BadRequest();
            }

            db.Entry(showing).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShowingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Concerts
        [ResponseType(typeof(Showing))]
        public IHttpActionResult PostShowing(Showing showing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Showings.Add(showing);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = showing.Id }, showing);
        }

        // DELETE: api/Concerts/5
        [ResponseType(typeof(Showing))]
        public IHttpActionResult DeleteShowing(int id)
        {
            Showing showing = db.Showings.Find(id);
            if (showing == null)
            {
                return NotFound();
            }

            db.Showings.Remove(showing);
            db.SaveChanges();

            return Ok(showing);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShowingExists(int id)
        {
            return db.Showings.Count(e => e.Id == id) > 0;
        }
    }
}