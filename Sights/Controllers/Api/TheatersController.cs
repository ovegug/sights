﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Sights.Models;

namespace Sights.Controllers
{
    //[RoutePrefix("api/Theaters")]
    public class TheatersController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("api/Seances/Theaters")]
        public IQueryable<Seance> GetSeance(int id)
        {
            return db.Seances.Where(s => s.ShowingId == id);
        }

        [Route("api/Locations/Theaters/{id}")]
        public IQueryable<ShowingDto> GetLocationShowings(int id)
        {
            var showings = db.Locations.Where(l => l.Type == "Театр" && l.Id == id).
                SelectMany(l => l.Seances.Select(s => s.Showing)).ToList();

            return showings.Select(s => ShowingDto.CreateFrom(s)).AsQueryable();
        }

        [Route("api/Locations/Theaters")]
        public IQueryable<Location> GetLocations()
        {
            return db.Locations.Where(l => l.Type == "Театр");
        }

        // GET: api/Theaters
        public IQueryable<ShowingDto> GetTheaters()
        {
            DateTime today = DateTime.Today;
            DateTime tomorrow = DateTime.Today.AddDays(1);

            var result = db.Showings.Where(s => s.Seances.Any(seance => seance.Time >= today && seance.Time < tomorrow)).ToArray();

            return result.Where(s => s.Type == "Театр").Select(s => ShowingDto.CreateFrom(s)).AsQueryable();
        }

        [Route("api/AzureTime")]
        public string GetAzureTime()
        {
            return DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }

        // GET: api/Theaters/5
        [ResponseType(typeof(Showing))]
        public IHttpActionResult GetTheater(int id)
        {
            Showing showing = db.Showings.Find(id);
            if (showing == null)
            {
                return NotFound();
            }

            return Ok(showing);
        }

        // PUT: api/Theaters/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShowing(int id, Showing showing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != showing.Id)
            {
                return BadRequest();
            }

            db.Entry(showing).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShowingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Theaters
        [ResponseType(typeof(Showing))]
        public IHttpActionResult PostShowing(Showing showing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Showings.Add(showing);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = showing.Id }, showing);
        }

        // DELETE: api/Theaters/5
        [ResponseType(typeof(Showing))]
        public IHttpActionResult DeleteShowing(int id)
        {
            Showing showing = db.Showings.Find(id);
            if (showing == null)
            {
                return NotFound();
            }

            db.Showings.Remove(showing);
            db.SaveChanges();

            return Ok(showing);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShowingExists(int id)
        {
            return db.Showings.Count(e => e.Id == id) > 0;
        }
    }
}