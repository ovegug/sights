﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Sights.Models;

namespace Sights.Controllers
{
    //[RoutePrefix("api/Museums")]
    public class MuseumsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("api/Seances/Museums")]
        public IQueryable<Seance> GetSeance(int id)
        {
            return db.Seances.Where(s => s.ShowingId == id);
        }


        [Route("api/Locations/Museums/{id}")]
        public IQueryable<ShowingDto> GetLocationShowings(int id)
        {
            var showings = db.Locations.Where(l => l.Type == "Музей" && l.Id == id).
                SelectMany(l => l.Seances.Select(s => s.Showing)).ToList();

            return showings.Select(s => ShowingDto.CreateFrom(s)).AsQueryable();
        }


        [Route("api/Locations/Museums")]
        public IQueryable<Location> GetLocations()
        {
            return db.Locations.Where(l => l.Type == "Музей");
        }

        // GET: api/Museums
        public IQueryable<ShowingDto> GetMuseums()
        {
            DateTime today = DateTime.Today;                    // earliest time today 
            DateTime tomorrow = DateTime.Today.AddDays(1);

            return db.Showings
                .Where(s => s.Type == "Музей" && s.Seances.Any(seance => seance.Time >= today && seance.Time < tomorrow))
                .ToArray()
                .Select(s => ShowingDto.CreateFrom(s))
                .AsQueryable();
        }

        // GET: api/Museums/5
        [ResponseType(typeof(Showing))]
        public IHttpActionResult GetShowing(int id)
        {
            Showing showing = db.Showings.Find(id);
            if (showing == null)
            {
                return NotFound();
            }

            return Ok(showing);
        }

        // PUT: api/Museums/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutShowing(int id, Showing showing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != showing.Id)
            {
                return BadRequest();
            }

            db.Entry(showing).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ShowingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Museums
        [ResponseType(typeof(Showing))]
        public IHttpActionResult PostShowing(Showing showing)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Showings.Add(showing);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = showing.Id }, showing);
        }

        // DELETE: api/Museums/5
        [ResponseType(typeof(Showing))]
        public IHttpActionResult DeleteShowing(int id)
        {
            Showing showing = db.Showings.Find(id);
            if (showing == null)
            {
                return NotFound();
            }

            db.Showings.Remove(showing);
            db.SaveChanges();

            return Ok(showing);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ShowingExists(int id)
        {
            return db.Showings.Count(e => e.Id == id) > 0;
        }
    }
}