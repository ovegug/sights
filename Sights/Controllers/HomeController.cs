﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sights.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult Seance()
        {
            return PartialView();
        }

        public PartialViewResult Cinema()
        {
            return PartialView();
        }
        public PartialViewResult Theater()
        {
            return PartialView();
        }
        public PartialViewResult Concert()
        {
            return PartialView();
        }
        public PartialViewResult Museum()
        {
            return PartialView();
        }

        public PartialViewResult Map()
        {
            return PartialView();
        }

        public PartialViewResult Comments()
        {
            return PartialView();
        }
    }
}
